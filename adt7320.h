//! \brief Simulation of ADT7320 temperature sensor
//!
//!


#ifndef _ADT7320_H_
#define _ADT7320_H_

#include <stdint.h>

//! \brief Set the state of the Chip Select line
//!
//! \param[in] value 0 to bring the nCS line low (0 volts), any other value to bring it high (3.3 volts).
void ADT7320_nCS(uint8_t value);

//! \brief Perform 1 byte of SPI communications
//!
//! Transmits a byte to the sensor MSB first over the SPI channel. As the byte is transmitted a byte is received.
//!
//! \param[in] toSensor The byte to transmit to the sensor
//! \return The byte received from the sensor
uint8_t ADT7320_SPI(uint8_t toSensor);

#endif //_ADT7320_H_
