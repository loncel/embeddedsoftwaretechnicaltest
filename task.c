/* Loncel Technologies

   Embedded Software Engineer Technical Task

   Description:
    This problem requires you to communicate with a simulated integrated circuit. The IC is an 
    ADT7320 temperature sensor from Analog Devices. The interface between the software you will
    write and the IC is a virtual SPI bus described in ADT7320.h.

    The implementation of the virtual IC has been obfuscated, and you would not have access to it in
    the real world case.

    The temperatures returned from the virtual sensor vary sinusoidally with a 1-degree Celsius 
    random number added.

   Task:
    Fork the project
    Configure the temperature sensor to work in 16-bit mode with continuous sampling
    Read the temperature
    Display the returned temperature as a floating point number.
    You do not need to expose all the functionality of the temperature sensor (e.g. writing a complete
    driver), but you code should be written in a way that is expandable to support other features of 
    the sensor.
    Send us a Pull request

   Development Environment Requirements:
    This has been written with Visual Studio 2015, but should compile with any C99 compiler. You may use
    any development tools.
*/

#include "adt7320.h"

int main(void) {




  return 0;
}